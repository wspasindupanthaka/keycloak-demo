package com.devsecops.keycloakdemo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
public class WebController {

    @RequestMapping(value = "/hello-world", method = RequestMethod.GET)
    public ResponseEntity<String> helloWorld() {
        return ResponseEntity.ok("Hello World");
    }

}

